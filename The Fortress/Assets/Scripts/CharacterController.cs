using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class CharacterController : MonoBehaviour
{
    public float rapidezDesplazamiento = 5.0f;
    public Camera camaraPrimeraPersona;
    //public Transform Hand;
    public GameObject tostada1, tostada2, liquido, horno, gift, gift2, gift3, sorpresa, sorpresa2, sorpresa3, hornoTXT, carpaTXT, 
    tostadasTXT, StickersTXT;
    //public float fuerza;
    //private Vector3 Escala;
    //private bool EnMano;
    public Material mat;
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        //Escala = Object.transform.localScale;
    }

    
    void Update()
    {
        Movement();
    }

    public void Movement()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime; movimientoCostados *= Time.deltaTime;
        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Tostadora")
        {
            tostada1.SetActive(true);
            tostada2.SetActive(true);
            tostadasTXT.SetActive(true);
        }
        else if (collision.gameObject.tag == "Licuadora")
        {
            liquido.GetComponent<MeshRenderer>().material = mat;

        }
        else if (collision.gameObject.tag == "Horno")
        {
            horno.SetActive(true);
            hornoTXT.SetActive(true);
        }
        else if (collision.gameObject.tag == "GIFT")
        {
            gift.SetActive(false);
            sorpresa.SetActive(true);
        }
        else if (collision.gameObject.tag == "GIFT2")
        {
            gift2.SetActive(false);
            sorpresa2.SetActive(true);
        }
        else if (collision.gameObject.tag == "GIFT3")
        {
            gift3.SetActive(false);
            sorpresa3.SetActive(true);
        }
        else if (collision.gameObject.tag == "Carpa")
        {
            carpaTXT.SetActive(true);
        }
        else if (collision.gameObject.tag == "Sticker")
        {
            StickersTXT.SetActive(true);
        }
      


    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Carpa")
        {
            carpaTXT.SetActive(false);
        }
        else if (collision.gameObject.tag == "Sticker")
        {
            StickersTXT.SetActive(false);
        }
        else if (collision.gameObject.tag == "Horno")
        {
            hornoTXT.SetActive(false);
        }
        if (collision.gameObject.tag == "Tostadora")
        {
            tostadasTXT.SetActive(false);
        }


    }

}
