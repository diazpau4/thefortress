using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateGames : MonoBehaviour
{
    public GameObject GOW, GTA, SONIC, BRTZ, TOY;
    void Start()
    {
        
    }

    
    void Update()
    {
        
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "GOW")
        {
            GOW.SetActive(true);
            GTA.SetActive(false);
            SONIC.SetActive(false);
        }
        else if (collision.gameObject.tag == "GTA")
        {
            GTA.SetActive(true);
            GOW.SetActive(false);
            SONIC.SetActive(false);
        }
        else if(collision.gameObject.tag == "SONIC")
        {
            SONIC.SetActive(true);
        }
        else if (collision.gameObject.tag == "BRTZ")
        {
            BRTZ.SetActive(true);
        }
        else if (collision.gameObject.tag == "TOY")
        {
            TOY.SetActive(true);
        }

    }

    public void OnCollisionExit(Collision collision)
    {
            if (collision.gameObject.tag == "GOW")
            {
                GOW.SetActive(false);
                GTA.SetActive(false);
                SONIC.SetActive(false);
                BRTZ.SetActive(false);
                TOY.SetActive(false);
            }
            else if (collision.gameObject.tag == "GTA")
            {
                GTA.SetActive(false);
                GOW.SetActive(false);
                SONIC.SetActive(false);
                BRTZ.SetActive(false);
                TOY.SetActive(false);
            }
            else if (collision.gameObject.tag == "SONIC")
            {
                SONIC.SetActive(false);
                GTA.SetActive(false);
                GOW.SetActive(false);
                TOY.SetActive(false);
                BRTZ.SetActive(false);
            }
            else if (collision.gameObject.tag == "BRTZ")
            {
               SONIC.SetActive(false);
               GTA.SetActive(false);
               GOW.SetActive(false);
               TOY.SetActive(false);
               BRTZ.SetActive(false);
            }
        else if (collision.gameObject.tag == "TOY")
        {
            SONIC.SetActive(false);
            GTA.SetActive(false);
            GOW.SetActive(false);
            TOY.SetActive(false);
            BRTZ.SetActive(false);
        }

    }
}
