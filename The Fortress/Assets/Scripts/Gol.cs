using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gol : MonoBehaviour
{
    public AudioClip gol;
    public GameObject Red;
    void Start()
    {
        
    }

    void Update()
    {

    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "ball") 
        {
            Red.GetComponent<AudioSource>().PlayOneShot(gol);
        }
    }
    
}
