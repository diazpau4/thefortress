using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public int ObjCorrectos = 0;
    public bool silla, casco1, caso2, mantel, palanca, volante, licuadora;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }
    void Start()
    {

    }


    void Update()
    {
        if (ObjCorrectos == 7)
        {
            SceneManager.LoadScene("Credits");
        }
    }

    public void ChequearTipo(GameObject go)
    {
        if (go.GetComponent<ImportantObject>().elegirTipo == ImportantObject.tipoObj.Silla && silla == false)
        {
            ObjCorrectos++;
            silla = true;
        }
        if (go.GetComponent<ImportantObject>().elegirTipo == ImportantObject.tipoObj.Casco && (casco1 == false || caso2 == false))
        {
            ObjCorrectos++;

            if (casco1 == false)
            {
                casco1 = true;
            }
            else
            {
                caso2 = true;
            }
        }
        if (go.GetComponent<ImportantObject>().elegirTipo == ImportantObject.tipoObj.Licuadora &&  licuadora == false)
        {
            ObjCorrectos++;
            licuadora = true;
        }
        if (go.GetComponent<ImportantObject>().elegirTipo == ImportantObject.tipoObj.Mantel && mantel == false)
        {
            ObjCorrectos++;
            mantel = true;
        }
        if (go.GetComponent<ImportantObject>().elegirTipo == ImportantObject.tipoObj.Palanca && palanca == false)
        {
            ObjCorrectos++;
            palanca = true;
        }
        if (go.GetComponent<ImportantObject>().elegirTipo == ImportantObject.tipoObj.Volante && volante == false)
        {
            ObjCorrectos++;
            volante = true;
        }
    }

    public void ChequearCorrecto(GameObject go)
    {
        if (go.GetComponent<ImportantObject>().elegirTipo == ImportantObject.tipoObj.Silla && silla == true)
        {
            ObjCorrectos--;
            silla = false;
        }
        if (go.GetComponent<ImportantObject>().elegirTipo == ImportantObject.tipoObj.Casco && (casco1 == true || caso2 == true))
        {
            ObjCorrectos--;

            if (casco1 == true)
            {
                casco1 = false;
            }
            else
            {
                caso2 = false;
            }
        }
        if (go.GetComponent<ImportantObject>().elegirTipo == ImportantObject.tipoObj.Licuadora && licuadora == true)
        {
            ObjCorrectos--;
            licuadora = false;
        }
        if (go.GetComponent<ImportantObject>().elegirTipo == ImportantObject.tipoObj.Mantel && mantel == true)
        {
            ObjCorrectos--;
            mantel = false;
        }
        if (go.GetComponent<ImportantObject>().elegirTipo == ImportantObject.tipoObj.Palanca && palanca == true)
        {
            ObjCorrectos--;
            palanca = false;
        }
        if (go.GetComponent<ImportantObject>().elegirTipo == ImportantObject.tipoObj.Volante && volante == true)
        {
            ObjCorrectos--;
            volante = false;
        }
    }
}
