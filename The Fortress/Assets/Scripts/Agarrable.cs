using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Agarrable : MonoBehaviour
{
    public GameObject elObjeto;
    public Transform mano;

    private bool activo;

    
    void Update()
    {
        if (activo == true)
        {
            if (Input.GetMouseButtonDown(0))
            {
                elObjeto.transform.SetParent(mano);
                elObjeto.GetComponent<Rigidbody>().isKinematic = true;
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            elObjeto.transform.SetParent(null);
            elObjeto.GetComponent<Rigidbody>().isKinematic = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            activo = true;
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            activo = false;
        }
    }
}
